
`include "def_indie.v"

module test_fpga_indie;

reg clk;
reg [3:0] key;
reg [9:0] sw;
wire [6:0] hex0, hex1, hex2, hex3;
wire [7:0] ledg;
wire [9:0] ledr;

wire [3:0] vga_r, vga_g, vga_b;
wire vga_hs, vga_vs;
wire [17:0] sram_addr;
inout [15:0] sram_dq;
wire sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n;

sram SRam (
    sram_addr, sram_dq, sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n
);

FPGA_INDIE FPGAIndie (
    clk, key, sw, hex0, hex1, hex2, hex3, ledg, ledr,
    vga_r, vga_g, vga_b, vga_hs, vga_vs, sram_addr, sram_dq,
    sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n
);

wire vram_we = FPGAIndie.cpu.vram_we;
wire [1:0] cpu_state = FPGAIndie.cpu_state;
wire [11:0] pc = FPGAIndie.cpu.pc_in;
wire com_start = FPGAIndie.cpu.com_start;
wire pc_sel = FPGAIndie.cpu.pc_sel;
wire [11:0] pc_out = FPGAIndie.cpu.pc_out;
wire [11:0] pc_out_nxt = FPGAIndie.cpu.pc_out_nxt;
wire com_rst = FPGAIndie.cpu.com_rst;
wire [7:0] clk_onehot = FPGAIndie.cpu.clk_onehot;
wire [3:0] flags = FPGAIndie.cpu.alu_flags;
wire [3:0] flags_nxt = FPGAIndie.cpu.alu_flags_nxt;
wire [3:0] flags_keepdrive = FPGAIndie.cpu.aluw.flags_keepdrive;
wire [3:0] iflags = FPGAIndie.cpu.aluw.iflags;
wire [4:0] alu_ctrl = FPGAIndie.cpu.alu_ctrl;
wire [4:0] iinst = FPGAIndie.cpu.iinst;
wire [31:0] reginst = FPGAIndie.cpu.reginst;
wire [31:0] reginst_next = FPGAIndie.cpu.reginst_next;
wire [31:0] ram_data_src = FPGAIndie.cpu.ram_data_src;
wire [31:0] ram_addr_src = FPGAIndie.cpu.ram_addr_src;
wire [31:0] alu_in1 = FPGAIndie.cpu.alu_in1;
wire [31:0] alu_in2 = FPGAIndie.cpu.alu_in2;
wire [31:0] alu_out = FPGAIndie.cpu.alu_out;
wire [11:0] iaddrimm = FPGAIndie.cpu.iaddrimm;
wire vramaccess = FPGAIndie.cpu.vramaccess;
wire Swap = FPGAIndie.Swap;

wire    [15:0]  LED_data = {ledr[7:0], ledg};

wire [4:0] seg7_0, seg7_1, seg7_2, seg7_3;

always #30 clk = ~clk;

task KEY_ACTION;    /// KEY[key_idx] : 1 -> 0 -> 1 pulse generation
input [1:0] key_idx;
begin
    @(negedge clk);
    key[key_idx] <= 0;
    //$display($stime, " KEY[%d] : 1->0->1 pulse", key_idx);
    @(negedge clk);
    key[key_idx] <= 1;
    @(negedge clk);
    @(negedge clk);
end
endtask

task SHOW_REGISTERS;
    begin
        $write("REG%g(%h), ", 3, FPGAIndie.cpu.registers.regs[3]);
        $write("REG%g(%h)", 21, FPGAIndie.cpu.registers.regs[21]);
    end
endtask

task SHOW_CPU_STATUS;
input show_regs;
begin
    $write($stime, " CLK(%b) PC(%h) NZVC(%b%b%b%b) ", clk_onehot, pc_out, flags[3], flags[1], flags[0], flags[2]);
    if (show_regs) SHOW_REGISTERS();
    $display();
end
endtask

task LED_DISPLAY;
input   add_newline;
begin
    $write($stime, " (cpu_state:%h) [%s%h%s%h%s%h%s%h] %h (%b %b)",
        cpu_state,
        (seg7_3[4]) ? "*" : " ", seg7_3[3:0],
        (seg7_2[4]) ? "*" : " ", seg7_2[3:0],
        (seg7_1[4]) ? "*" : " ", seg7_1[3:0],
        (seg7_0[4]) ? "*" : " ", seg7_0[3:0],
        LED_data, LED_data[15:8], LED_data[7:0]);
    if(add_newline) $display();
end
endtask

initial begin
    clk = 0;
    key = 4'hf;
    @(negedge clk);
    @(negedge clk);
    //SHOW_CPU_STATUS(0);
    //LED_DISPLAY(1);
    KEY_ACTION(0);
    //#100 LED_DISPLAY(1);

    //#10000 $finish;

    if (pc == 12'h96 || pc == 12'h98) begin
        SHOW_CPU_STATUS(0);
        $finish;
    end
    //#900000000 $finish;
end

integer i;
reg [15:0] Data;
reg [7:0] Color;
reg [17:0] Addr;
always @(negedge clk) begin
    //if (FPGAIndie.Swap) begin
    //    for (i = 0; i < 320*240; i = i + 1) begin
    //        Data = SRam.mem[i];
    //        Addr = i;
    //        $display("@%h %h %h %h", Addr, {Color[7:5], 1'b0}, {Color[4:2], 1'b0}, {Color[1:0], 2'b0});
    //    end
    //end
    //$display("%h %h %b %b %b %b %b %h %b", sram_addr, sram_dq, sram_ce_n, sram_we_n, sram_oe_n, sram_lb_n, sram_ub_n,
    //    FPGAIndie.Controller.State, FPGAIndie.clock_25);
    $display("%h %h", sram_addr, sram_dq);
    //$display("@%h %h %h %h", FPGAIndie.vga_packed_addr, FPGAIndie.vga_r, FPGAIndie.vga_g, FPGAIndie.vga_b);
    //$write($stime, " PC(%h) ", pc);
    //SHOW_REGISTERS();
    //$write("\015");
end

endmodule

module seg7_enc (data, enc_out);
    input  [3:0] data;
    output [6:0] enc_out;
    reg    [6:0] enc_out_n;

    assign enc_out = ~enc_out_n;

    always @(data)
    case(data)
        4'h0:    enc_out_n = `SEG_7_0;
        4'h1:    enc_out_n = `SEG_7_1;
        4'h2:    enc_out_n = `SEG_7_2;
        4'h3:    enc_out_n = `SEG_7_3;
        4'h4:    enc_out_n = `SEG_7_4;
        4'h5:    enc_out_n = `SEG_7_5;
        4'h6:    enc_out_n = `SEG_7_6;
        4'h7:    enc_out_n = `SEG_7_7;
        4'h8:    enc_out_n = `SEG_7_8;
        4'h9:    enc_out_n = `SEG_7_9;
        4'ha:    enc_out_n = `SEG_7_A;
        4'hb:    enc_out_n = `SEG_7_B;
        4'hc:    enc_out_n = `SEG_7_C;
        4'hd:    enc_out_n = `SEG_7_D;
        4'he:    enc_out_n = `SEG_7_E;
        4'hf:    enc_out_n = `SEG_7_F;
        default: enc_out_n = `SEG_7_UNDEFINED;
    endcase
endmodule

module seg7_dec (hex, dec_out);
    input  [6:0] hex;
    output [4:0] dec_out;
    reg    [4:0] dec_out;

    always @(hex)
    case(~hex)
        `SEG_7_0 : dec_out = 5'h00;
        `SEG_7_1 : dec_out = 5'h01;
        `SEG_7_2 : dec_out = 5'h02;
        `SEG_7_3 : dec_out = 5'h03;
        `SEG_7_4 : dec_out = 5'h04;
        `SEG_7_5 : dec_out = 5'h05;
        `SEG_7_6 : dec_out = 5'h06;
        `SEG_7_7 : dec_out = 5'h07;
        `SEG_7_8 : dec_out = 5'h08;
        `SEG_7_9 : dec_out = 5'h09;
        `SEG_7_A : dec_out = 5'h0a;
        `SEG_7_B : dec_out = 5'h0b;
        `SEG_7_C : dec_out = 5'h0c;
        `SEG_7_D : dec_out = 5'h0d;
        `SEG_7_E : dec_out = 5'h0e;
        `SEG_7_F : dec_out = 5'h0f;
        default  : dec_out = 5'h1f;
    endcase
endmodule

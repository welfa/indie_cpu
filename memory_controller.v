module VRAM_CONTROLLER (
    input clk, reset,
    
    // From VGA
    input [9:0] PixelX, PixelY,
    input clk25,
    // Memory to VGA Read
    output [7:0] VGAData,

    // Memory to CPU
    input [16:0] CPUAddress,
    input [7:0] CPUData,
    input CPUWrite,
	 input Swap,

    // To/From SRAM
    output [17:0] SRAM_ADDR,
    inout wire [15:0] SRAM_DQ,
    output SRAM_WE_N, SRAM_OE_N, SRAM_UB_N, SRAM_LB_N, SRAM_CE_N
);

parameter
    Idle = 2'h0,
    WaitWrite = 2'h1,
    Write = 2'h2;

reg [1:0] State, NextState;
reg [7:0] VGAReadData;
reg [16:0] CPUAddressReg, CPUAddressNext;
reg [7:0] WriteData, WriteDataNext;
reg WriteEnable, Buffer;
wire WriteEnableNext;
wire [17:0] YOffset;
wire [17:0] VGAAddress, MemoryAddress;
wire [7:0] MemoryData;
wire VGACycle;

assign VGACycle = clk25;

// address = 320*(y/2) + x/2 = 256*(y/2) + 64*(y/2) + x/2
assign YOffset = {1'b0, PixelY[9:1], 8'b0} + {3'b0, PixelY[9:1], 6'b0};
assign VGAAddress = YOffset + {9'b0, PixelX[9:1]};
assign VGAData = VGAReadData;

always @(posedge clk) begin
    if (VGACycle == 1)
        VGAReadData <= MemoryData;
end

always @(posedge clk) begin
    if (reset == 1'b1) begin
        State <= Idle;
        CPUAddressReg <= 16'b0;
        WriteData <= 16'b0;
        WriteEnable <= 1'b1;
		  Buffer <= 0;
    end else if (clk == 1'b1) begin
        State <= NextState;
        CPUAddressReg <= CPUAddressNext;
        WriteData <= WriteDataNext;
        WriteEnable <= WriteEnableNext;
    end
end

always @* begin
    NextState <= State;
    CPUAddressNext <= CPUAddressReg;
    WriteDataNext <= WriteData;
    case (State)
        Idle:
            if (CPUWrite) begin
                CPUAddressNext <= CPUAddress;
                WriteDataNext <= CPUData;
                if (VGACycle == 1) begin
                    NextState <= Write;
                end else begin
                    NextState <= WaitWrite;
                end
            end
        WaitWrite:
            NextState <= Write;
        Write:
            NextState <= Idle;
    endcase
end

assign WriteEnableNext = (NextState == Write) ? 0 : 1'b1;

assign MemoryAddress = (VGACycle == 1) ? VGAAddress :
    (WriteEnable == 0) ? CPUAddressReg : CPUAddress;
assign SRAM_ADDR = {1'b0, MemoryAddress[17:1]};
assign SRAM_CE_N = 0;
assign SRAM_OE_N = 0;
assign SRAM_WE_N = WriteEnable;
assign SRAM_LB_N = MemoryAddress[0];
assign SRAM_UB_N = ~MemoryAddress[0];
assign SRAM_DQ = (WriteEnable == 0) ? (MemoryAddress[0]) ? {WriteData, 8'b0} : {8'b0, WriteData} : 16'bZ;

assign MemoryData = (MemoryAddress[0]) ? SRAM_DQ[15:8] : SRAM_DQ[7:0];

endmodule

`include "def_indie.v"

module CPU_INDIE (
    input               clk_cpu_orig,
    input       [1:0]   com_ctl,
    output              vram_swp,
    output      [31:0]  vram_addr,
    output      [7:0]   vram_data,
    output              vram_we,
    output      [15:0]  led_out,
    output reg  [15:0]  debug_out);

    assign      led_out[15:0] = led_reg;

    wire    com_rst    = (com_ctl == `COM_RST); /// reset
    wire    com_start  = (com_ctl == `COM_RUN); /// release all resets

    assign
        vram_addr = vramaccess ? reg_out_dst - 32'd4096 : 32'b0,
        vram_data = vramaccess ? alu_out : 8'b0,
        vram_we   = clk_onehot[2] & vramaccess;

    reg [15:0] led_reg = 16'b0;

    wire        clk_main, clk_mult;
    wire [7:0]  clk_onehot, clk_onehot_rd;
    CLK_MANAGER clocker (
        clk_cpu_orig,
        clk_mult,
        clk_main,
        clk_onehot, clk_onehot_rd,
        com_rst);

    //ramのアドレスをマルチプレクサで切り替える際，
    //切り替えがramと同じタイミングで同期していると怪しいので
    //半分周期を早めたヤバクロックに同期させて切り替える
    wire [11:0]
        ram_addr_src = clk_onehot_rd[0] ? pc_out_nxt :  //posedge [0]に間に合わせる
            RAMMLTPLXR(ram_sel_src, iaddrimm, reg_out_src),
        ram_addr_dst =
            RAMMLTPLXR(
                vramaccess ? 2'b10 : ram_sel_dst,
                iaddrimm,
                reg_out_dst);
    function [31:0] RAMMLTPLXR (
        input [1:0]     rm_sel,
        input [31:0]    rm_imm, rm_reg);
        case (rm_sel)
            2'b00:  RAMMLTPLXR = rm_imm;
            2'b01:  RAMMLTPLXR = rm_reg;
            2'b10:  RAMMLTPLXR = 12'h000;
            default:RAMMLTPLXR = 12'hxxx;
        endcase
    endfunction

    wire [31:0]     ram_data_src, ram_data_dst;
    DPRAM_SYNC ram (
        alu_out,
        ram_addr_src,   ram_addr_dst,
        ram_we,         clk_mult,
        ram_data_src,   ram_data_dst);

    wire [31:0] reginst, reginst_next;
    REG_LCI_NXT reg_inst (
        clk_mult, `c1,
        ram_data_src,
        reginst, reginst_next,
        clk_onehot[0], com_rst, `c0);

    wire [4:0]  iinst;
    wire [1:0]  iref_src,        iref_dst;
    wire [4:0]  ireg_addr_src,   ireg_addr_dst;
    wire [11:0] iaddrimm;
    INST_PARSER parser (
        clk_onehot[0] ? reginst_next : reginst,
        iinst,
        iref_src,       iref_dst,
        ireg_addr_src,  ireg_addr_dst,
        iaddrimm);

    wire    [1:0]   ram_sel_src, ram_sel_dst;
    //0 for M[reg[ad]], 1 for add/imm, 2 for reg[ad], 3 for 0
    wire    [1:0]   alu_sel_src, alu_sel_dst;
    //ほぼinst
    wire    [4:0]   alu_ctrl;
    wire            tram_we, ram_we, reg_we, vramaccess;
    assign          ram_we = vramaccess ? 1'b0 : tram_we;
    MAIN_CONTROLLER ctrlr (
        iinst,
        clk_onehot,
        iref_src, iref_dst,
        ram_sel_src, ram_sel_dst,
        alu_sel_src, alu_sel_dst,
        reg_out_dst,
        vramaccess,
        alu_ctrl,
        tram_we, reg_we,
        vram_swp);

    wire [31:0] reg_out_src, reg_out_dst;
    wire [31:0] reg_din_dst = alu_out;
    GEN_REGS registers (
        ireg_addr_src, ireg_addr_dst,
        reg_din_dst,
        reg_we,
        clk_mult,
        reg_out_src, reg_out_dst,
        com_rst);

    wire [31:0] alu_in1 =
        ALUMLTPLXR(vramaccess ? 2'b11 : alu_sel_dst, ram_data_dst, iaddrimm, reg_out_dst);
    wire [31:0] alu_in2 =
        ALUMLTPLXR(
            alu_sel_src,
            ram_data_src,
            iaddrimm,
            reg_out_src);
    wire [31:0] alu_out;
    wire [3:0]  alu_flags, alu_flags_nxt;

    ALU_WRAPPER aluw (
        alu_ctrl,
        alu_in1, alu_in2,
        clk_mult,
        clk_onehot,
        alu_out,
        alu_flags,
        alu_flags_nxt,
        com_rst);
    function [31:0] ALUMLTPLXR (
        input [1:0]     am_sel,
        input [31:0]    am_ram, am_imm, am_reg);
        case (am_sel)
            2'b00:  ALUMLTPLXR = am_ram;
            2'b01:  ALUMLTPLXR = am_imm;
            2'b10:  ALUMLTPLXR = am_reg;
            2'b11:  ALUMLTPLXR = 32'h00000000;
            default:ALUMLTPLXR = 32'h0badbeef;
        endcase
    endfunction

    wire    pc_sel;
    PC_CONTROLLER pc_ctrlr(
        iinst,
        alu_flags_nxt,  //Neg Zero oVerflow Clear
        pc_sel);        //0 for PC+4,   1 for imm
    wire [11:0] pc_in, pc_out, pc_out_nxt;
    assign      pc_in = pc_sel ? iaddrimm : pc_out + 12'h01;
    REG_LCI_NXT #12 reg_pc (
        clk_mult, `c1,
        pc_in,
        pc_out, pc_out_nxt,
        clk_onehot[2], com_rst, `c0); //posedge clk_onehot[0]で書かれる
endmodule


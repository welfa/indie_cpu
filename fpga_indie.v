`include "def_indie.v"

module FPGA_INDIE (
    ////////////////////    Clock Input     ////////////////////     
    input           clock_50,               //  27 MHz
    ////////////////////    Push Button     ////////////////////
    input   [3:0]   key,                    //  Pushbutton[3:0]
    ////////////////////    DPDT Switch     ////////////////////
    input   [9:0]   sw,                     //  Toggle Switch[9:0]
    ////////////////////    7-SEG Display   ////////////////////
    output  [6:0]   hex0,hex1,hex2,hex3,
    ////////////////////////    LED     ////////////////////////
    output  [7:0]   ledg,                   //  LED Green[7:0]
    output  [9:0]   ledr,                   //  LED Red[9:0]
    ////////////////////////    VGA     ////////////////////////
    output  [3:0]   vga_r,  vga_g,  vga_b,  //  VGA RGB
    output          vga_hs, vga_vs,         //  VGA H/V sync
    ////////////////////////    SRAM    ////////////////////////
    output  [17:0]  sram_addr,
    inout   [15:0]  sram_dq,
    output          sram_we_n, sram_oe_n,
                    sram_ub_n, sram_lb_n, sram_ce_n);

    wire VideoOn;
    assign {vga_r, vga_g, vga_b} = (VideoOn) ? {v_dout[7:5], v_dout[5], v_dout[4:2], v_dout[2], v_dout[1:0], v_dout[0], v_dout[0]} :
        12'b0;

    reg  clock_25 = 1'b0;
    always @(posedge clock_50) begin
        clock_25 <= ~clock_25;
    end
    wire    [31:0]  vga_addr;//vga_Rnum,   vga_Cnum;
    wire [9:0] vga_Rnum, vga_Cnum;
    wire    [7:0]   vga_idata,  vga_odata;
    VGA vga(
        clock_25, com_rst,
        vga_hs, vga_vs,
        VideoOn,
        ,
        vga_Cnum,
        vga_Rnum);

    wire    [7:0]   v_dout;

    wire [31:0] vram_addr;
    wire [7:0]  vram_data;
    wire        vram_we;

    assign sys_probe_data[15:14] = {sram_we_n, sram_oe_n};

    wire Swap;
    wire    com_rst    = (cpu_state == `COM_RST); /// reset
    VRAM_CONTROLLER Controller(
        clk, com_rst,
        vga_Cnum, vga_Rnum,
        clock_25,
        v_dout,
        vram_addr[16:0],
        vram_data,
        vram_we,
		  Swap,
        sram_addr,
        sram_dq,
        sram_we_n, sram_oe_n, sram_ub_n, sram_lb_n, sram_ce_n
    );

    wire    [15:0]  sp;

    dec_4to16 dec_p (sw[5:2], sp, 1'b1);

    wire    [17:0]  sys_probe_data;
    assign          ledg = sys_probe_data[7:0];
    assign          ledr = sys_probe_data[17:8];


    wire    [3:0]   prev_key;
    wire    [1:0]   cpu_state;


    wire [15:0] led_out;
    SEG7_ENC seg7_e0 (led_out[15:12], hex3);
    SEG7_ENC seg7_e1 (led_out[11: 8], hex2);
    SEG7_ENC seg7_e2 (led_out[ 7: 4], hex1);
    SEG7_ENC seg7_e3 (led_out[ 3: 0], hex0);

    wire [15:0] debug_out;
    assign sys_probe_data[13:0] = (sp[0])  ? debug_out[13:0] : 14'h3fff;
    assign sys_probe_data[17:16] = cpu_state;

    reg     clock_200;
    wire    clk = clock_50;
    FPGA_EX3_FSM fsm (clk ,key, cpu_state);
    reg [31:0] count = 32'b0;
    always @(posedge clock_50) begin
        count <= count+1;
        if(count==32'd800000) begin
            clock_200 <= ~clock_200;
            count <= 0;
        end
    end

    assign led_out[7:0] = v_dout;
    CPU_INDIE cpu(
        clk,
        cpu_state,
        Swap,
        vram_addr,
        vram_data,
        vram_we,
        ,
        debug_out);
endmodule

module FPGA_EX3_FSM (
    input               clk,
    input       [3:0]   key,
    output  reg [1:0]   cpu_state);

    reg     [3:0]   prev_key;

    /// detect 1->0 transition on KEY[n]
    wire    key0    = ~key[0] & prev_key[0];
    wire    key1    = ~key[1] & prev_key[1];
    wire    key2    = ~key[2] & prev_key[2];
    wire    key3    = ~key[3] & prev_key[3];

    initial begin
        cpu_state = `COM_RST; ///   cpu_state = S0 after power-on
        $display("cpu state = RST");
    end
    initial prev_key  = 4'b0;     ///   PREV_KEY = 0000 after power-on

////////////       system finite state machine       //////////////
    always @ (posedge clk) begin
        prev_key <= key;
        case(cpu_state)
//////////// COM_RST : reset state (after power on) //////////// 
            `COM_RST : begin
                if(key0) begin
                    cpu_state <= `COM_RUN;
                    $display("cpu state = RUN");
                end
            end
//////////// COM_RUN : run state //////////// 
            `COM_RUN : begin
                if(key0) begin
                    cpu_state <= `COM_RST;
                    $display("cpu state = RST");
                end
            end
        endcase
    end
endmodule


`include "constants.v"

module alu(primaryOperand, secondaryOperand, operation, result, flags);

	input	     [31:0]	primaryOperand;
	input 	     [31:0]	secondaryOperand;
	input 	     [4:0]	operation;
	output	     [31:0]	result;
	output	     [3:0]	flags;

    wire [32:0] AddResult = (operation == `IADD) ? ({1'b0, primaryOperand} + {1'b0, secondaryOperand}) : 33'b0;
    wire [32:0] SubResult = (operation == `ISUB) ?
        ({primaryOperand[31], primaryOperand} - {secondaryOperand[31], secondaryOperand}) : 33'b0;
    wire [32:0] CmpResult = (operation == `ICMP) ?
        ({primaryOperand[31], primaryOperand} - {secondaryOperand[31], secondaryOperand}) : 33'b0;
    wire [64:0] MulResult = (operation == `IMUL) ? ({33'b0, primaryOperand} * {33'b0, secondaryOperand}) : 65'b0;
    wire [31:0] AndResult = (operation == `IAND) ? primaryOperand & secondaryOperand : 32'b0;
    wire [31:0] ClrResult = 32'b0;
    wire [31:0] DivResult = (operation == `IDIV) ? primaryOperand / secondaryOperand : 32'b0;
    wire [31:0] XorResult = (operation == `IXOR) ? primaryOperand ^ secondaryOperand : 32'b0;
    wire [31:0] NegResult = (operation == `INEG) ? -primaryOperand : 32'b0;
    wire [31:0] NotResult = (operation == `INOT) ? ~primaryOperand : 32'b0;
    wire [31:0] OrResult = (operation == `IOR) ? primaryOperand | secondaryOperand : 32'b0;
    wire [31:0] AslResult = (operation == `IASL) ? primaryOperand << secondaryOperand : 32'b0;
    wire [31:0] AsrResult = (operation == `IASR) ? primaryOperand >> secondaryOperand : 32'b0;

    assign result = AddResult[31:0] | SubResult[31:0] | MulResult[31:0] | AndResult | DivResult | XorResult |
        NegResult | NotResult | OrResult | AslResult | AsrResult;

    wire [31:0] AllResults = AddResult[31:0] | SubResult[31:0] | MulResult[31:0] | AndResult | DivResult | XorResult |
        NegResult | NotResult | OrResult | AslResult | AsrResult | CmpResult[31:0];

    assign flags[`CARRYFLAG] = (AddResult[32] | SubResult[32] | (MulResult[64:32] != 0) | CmpResult[32]);

    assign flags[`NEGFLAG] = (operation == `ICLR) ? 1'b0 : AllResults[31];

    assign flags[`ZEROFLAG] = (AllResults[31:0] == 32'h0) ? 1'b1 : 1'b0;

    assign flags[`OVERFLOWFLAG] = 
        (operation == `IADD) ? flags[`CARRYFLAG] ^ AddResult[31] :
        (operation == `ISUB) ? flags[`CARRYFLAG] ^ SubResult[31] :
        (operation == `ICMP) ? flags[`CARRYFLAG] ^ CmpResult[31] :
        (operation == `IMUL) ? ((primaryOperand[31] == ~secondaryOperand[31]) & (result[31] == 0)) | ((primaryOperand[31] == secondaryOperand[31]) & (result[31] == 1)) : 
        (operation == `INEG) ? (primaryOperand == `MGB) : 
        1'b0;

endmodule

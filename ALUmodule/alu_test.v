`timescale 10ps/10ps
`include "../def_indie.v"
`include "constants.v"

module and_test;
  reg [31:0] primaryOperand; 
  reg [31:0] secondaryOperand;
  reg [4:0]  operation;
  wire [31:0] result;
  wire [3:0]  flags;

  alu alu(primaryOperand, secondaryOperand, operation, result, flags);

  wire [32:0] CmpResult = alu.CmpResult;

  initial
    begin
      $monitor($time, ": primaryOperand=%h, secondaryOperand=%h, operation=%h, result=%h, flags=%b, CmpResult=%h, NEG=%b,%b", primaryOperand, secondaryOperand, operation, result, flags, CmpResult, CmpResult[31], flags[`NEGFLAG]);
      #1 primaryOperand=0; secondaryOperand=1; operation=`ICMP;
      #1 primaryOperand = 3; secondaryOperand = 1; operation = `ISUB;
    end
endmodule

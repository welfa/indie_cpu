module sram(address, data, we, oe, ub, lb, ce);

	input	[17:0]		address;
	input 				we; //write enable
	input				oe; //output enable
	input 				ub; //high-byte data mask
	input    		   	lb; //low-byte data mask 
	input 				ce; //chip enable

	inout 	[15:0]		data;

	reg		[15:0]  	mem[0:512*1024-1]; //memmory

    reg [15:0] DataBusDriver;

    wire [15:0] data = DataBusDriver;

    integer i;
    initial begin
        DataBusDriver = 16'hZZZZ;
        for (i = 0; i < 512*1024; i = i + 1) begin
            mem[i] = 0;
        end
    end

    always @(ce or we or oe or lb or ub) begin
        if (ce == 1'b0) begin
            if (we == 1'b0) begin
                DataBusDriver <= #10 16'hZZZZ;
                #11;
                if (ub & !lb) begin
                    $display($time, " Writing LB Address=%h Data=%h (Full=%h)", address, data[7:0], data);
                    mem[address][7:0] = data[7:0];
                end else if (!ub & lb) begin
                    $display($time, " Writing UB Address=%h Data=%h (Full=%h)", address, data[15:8], data);
                    mem[address][15:8] = data[15:8];
                end else begin
                    $display($time, " Writing Address=%h Data=%h", address, data);
                    mem[address] = data;
                end
            end else if (oe == 1'b0) begin
                #6 DataBusDriver = mem[address];
                //$display($time, " Reading Address=%h Data=%h", address, DataBusDriver);
            end
        end else begin
            DataBusDriver <= #10 16'hZZZZ;
        end
    end

endmodule

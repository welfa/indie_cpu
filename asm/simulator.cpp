#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include "SDL.h"

#define internal static
#define global_variable static
#define local_persist static

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef s32 bool32;

typedef float real32;
typedef double real64;

struct string_table {
    u8 *Base;
    s32 SizeLeft;
};

internal string_table InitializeStringTable() {
    string_table Result;
    Result.Base = (u8 *)malloc(1 << 24);
    assert(Result.Base);
    Result.SizeLeft = 1 << 24;

    return Result;
}

internal char *PushStringTable(string_table *StringTable, s32 Size) {
    assert(StringTable->SizeLeft >= Size);
    char *Result = (char *)StringTable->Base;
    StringTable->Base += Size;
    StringTable->SizeLeft -= Size;

    return Result;
}

global_variable u32 Memory[1 << 14];
global_variable u32 Register[32];
global_variable u8 VRam[320*240];

int main(int ArgCount, char *Arguments[]) {
    if (ArgCount > 2 || ArgCount == 1) {
        printf("Simulator only accepts 1 file!\n");
        return -1;
    }

    string_table StringTable = InitializeStringTable();

    char *Filename = Arguments[1];
    struct stat FileStat;

    int FileDescriptor = open(Filename, O_RDONLY);
    if (FileDescriptor < 0) {
        perror("Error opening file: ");
        exit(1);
    }
    if (fstat(FileDescriptor, &FileStat) < 0) {
        perror("Error getting stat of file: ");
        exit(1);
    }

    s32 FileSize = FileStat.st_size;
    char *Contents = PushStringTable(&StringTable, FileSize);
    if (read(FileDescriptor, Contents, FileSize) != FileSize) {
        perror("Error reading file: ");
        exit(1);
    }
    close(FileDescriptor);

    s32 MaxAddress = 0;
    for (s32 CharIndex = 0; CharIndex < FileSize; CharIndex++) {
        char Buffer[32];
        s32 BufferIndex = 0;
        while (Contents[CharIndex] != '\n') {
            assert(BufferIndex < ArrayCount(Buffer));
            Buffer[BufferIndex] = Contents[CharIndex];
            BufferIndex++;
            CharIndex++;
        }
        s32 Address, Data;
        sscanf(Buffer, "@%3x\t%8x", &Address, &Data);
        assert(Address < ArrayCount(Memory));
        Memory[Address] = Data;
        if (Address > MaxAddress) {
            MaxAddress = Address;
        }
    }

    assert(SDL_Init(SDL_INIT_VIDEO) != -1);

    SDL_Surface *Screen = SDL_SetVideoMode(640, 480, 0, SDL_SWSURFACE);

    // Start executing
    s32 ProgramCounter = 0;
    bool32 N = false, Z = false, V = false, C = false;
    u64 Cycle = 0;
    for (;;) {
        assert(ProgramCounter <= MaxAddress);
        s32 Instruction = Memory[ProgramCounter++];

        u32 SourceData, DestData;
        u32 *DestWrite = 0;
        u8 *DestVRam = 0;
        s32 Operator = (Instruction & 0xF8000000) >> 27;
        if (ProgramCounter == 9) {
            int k = 1;
        }
        //printf("PC=%x %d\n", ProgramCounter - 1, Operator);
        s32 Source = (Instruction & 0x07F00000) >> 20;
        s32 SourceType = (Source & 0x60) >> 5;
        s32 Dest = (Instruction & 0x000FE000) >> 13;
        s32 DestType = (Dest & 0x60) >> 5;
        s32 AddOrImm = (Instruction & 0xFFF) >> 0;
        if (SourceType == 0) {
            assert((Source & 0x3F) < ArrayCount(Register));
            SourceData = Register[(Source & 0x3F)];
        } else if (SourceType == 1) {
            assert(AddOrImm < ArrayCount(Memory));
            SourceData = Memory[AddOrImm];
        } else if (SourceType == 2) {
            assert((Source & 0x3F) < ArrayCount(Register));
            s32 MemoryAddress = Register[(Source & 0x3F)];
            assert(MemoryAddress < ArrayCount(Memory));
            SourceData = Memory[MemoryAddress];
        } else if (SourceType == 3) {
            SourceData = AddOrImm;
        } else {
            assert(false);
        }
        if (DestType == 0) {
            assert((Dest & 0x3F) < ArrayCount(Register));
            DestData = Register[(Dest & 0x3F)];
            DestWrite = Register + (Dest & 0x3F);
        } else if (DestType == 1) {
            assert(AddOrImm < ArrayCount(Memory));
            DestData = Memory[AddOrImm];
            DestWrite = Memory + AddOrImm;
        } else if (DestType == 2) {
            assert((Dest & 0x3F) < ArrayCount(Register));
            s32 MemoryAddress = Register[(Dest & 0x3F)];
            if (MemoryAddress > 4096) {
                MemoryAddress = MemoryAddress - 4096;
                if (MemoryAddress < ArrayCount(VRam) && MemoryAddress >= 0) {
                } else {
                    printf("%d\n", MemoryAddress);
                }
                assert(MemoryAddress < ArrayCount(VRam) && MemoryAddress >= 0);
                //printf("%d\n", MemoryAddress);
                DestData = VRam[MemoryAddress];
                DestVRam = VRam + MemoryAddress;
            } else {
                if (MemoryAddress < ArrayCount(Memory)) {
                } else {
                    printf("%x\n", MemoryAddress);
                    assert(MemoryAddress < ArrayCount(Memory));
                }
                DestData = Memory[MemoryAddress];
                DestWrite = Memory + MemoryAddress;
            }
        } else {
            assert(false);
        }
        switch (Operator) {
            case 0x00:
                {
                    u64 Result = (u64)DestData + (u64)SourceData;
                    C = (bool32)((Result & 0xFFFF0000) > 0);
                    N = ((s32)Result) < 0;
                    V = ((s32)DestData > 0 && (s32)SourceData > 0 && N) ||
                        ((s32)DestData < 0 && (s32)SourceData < 0 && !N);
                    Z = (Result == 0);
                    *DestWrite = (u32)Result;
                } break;
            case 0x01:
                {
                    u32 Result = DestData & SourceData;
                    C = 0;
                    V = 0;
                    N = (Result & 0x8000) > 0;
                    Z = (Result == 0);
                    *DestWrite = Result;
                } break;
            case 0x02:
                {
                    u64 Result = (u64)(DestData) << SourceData;
                    Z = (((u32)Result) == 0);
                    *DestWrite = (u32)Result;
                } break;
            case 0x03:
                {
                    u64 Result = (u64)(DestData) >> SourceData;
                    Z = (((u32)Result) == 0);
                    *DestWrite = (u32)Result;
                } break;
            case 0x04:
                {
                    if (!C) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x05:
                {
                    if (C) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x06:
                {
                    if (Z) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x07:
                {
                    if ((N && V) || (!N && !V)) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x08:
                {
                    if ((N && V && !Z) || (!N && !V && !Z)) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x09:
                {
                    if (Z || (N && !V) || (!N && V)) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0A:
                {
                    if ((N && !V) || (!N && V)) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0B:
                {
                    if (N) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0C:
                {
                    if (!Z) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0D:
                {
                    if (!N) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0E:
                {
                    if (!V) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x0F:
                {
                    if (V) {
                        ProgramCounter = AddOrImm;
                    }
                } break;
            case 0x10:
                {
                    ProgramCounter = AddOrImm;
                } break;
            case 0x11:
                {
                    *DestWrite = 0;
                    Z = 1;
                    N = 0;
                    V = 0;
                    C = 0;
                } break;
            case 0x12:
                {
                    u32 Result = DestData + (u32)(-(s32)SourceData);
                    N = ((s32)Result < 0);
                    Z = (Result == 0);
                    V = ((s32)DestData > 0 && (s32)SourceData < 0 && N) ||
                        ((s32)DestData < 0 && (s32)SourceData > 0 && !N);
                    // TODO: Carry flag
                } break;
            case 0x13:
                {
                    u32 Result = DestData / SourceData;
                    C = 0;
                    N = (Result < 0);
                    Z = (Result == 0);
                    V = ((s32)DestData > 0 && (s32)SourceData > 0 && N) ||
                        ((s32)DestData < 0 && (s32)SourceData < 0 && !N);
                    *DestWrite = Result;
                } break;
            case 0x14:
                {
                    u32 Result = DestData ^ SourceData;
                    V = 0;
                    C = 0;
                    Z = (Result == 0);
                    N = ((s32)Result < 0);
                    *DestWrite = Result;
                } break;
            case 0x15:
                {
                    // TODO: Empty
                } break;
            case 0x16:
                {
                    // TODO: CALL
                } break;
            case 0x17:
                {
                    // TODO: RET
                    SDL_FillRect(Screen, NULL, 0xFFFF00);
                    if (SDL_MUSTLOCK(Screen)) {
                        if (SDL_LockSurface(Screen) < 0) {
                            assert(false);
                        }
                    }
                    u32 *TargetPixel = (u32 *)Screen->pixels;
                    for (s32 Y = 479; Y > 0; Y--) {
                        for (s32 X = 639; X > 0; X--) {
                            u8 Pixel = VRam[(X/2) + (Y/2)*320];
                            u8 R = (u8)(((real32)((Pixel & 0xE0) >> 5)/7)*255);
                            u8 G = (u8)(((real32)((Pixel & 0x1C) >> 2)/7)*255);
                            u8 B = (u8)(((real32)(Pixel & 0x03)/3.0f)*255);
                            u32 Color = SDL_MapRGB(Screen->format, R, G, B);

                            TargetPixel[(Y*Screen->w) + X] = Color;
                        }
                    }
                    if (SDL_MUSTLOCK(Screen)) {
                        SDL_UnlockSurface(Screen);
                    }
                    SDL_UpdateRect(Screen, 0, 0, 0, 0);
                } break;
            case 0x18:
                {
                    if (DestVRam) {
                        *DestVRam = (u8)SourceData;
                    } else {
                        *DestWrite = SourceData;
                    }
                    C = 0;
                    V = 0;
                    N = (SourceData < 0);
                    Z = (SourceData == 0);
                } break;
            case 0x19:
                {
                    u32 Result = DestData * SourceData;
                    C = 0;
                    N = (Result < 0);
                    Z = (Result == 0);
                    V = ((s32)DestData > 0 && (s32)SourceData > 0 && N) ||
                        ((s32)DestData < 0 && (s32)SourceData < 0 && !N);
                    *DestWrite = Result;
                } break;
            case 0x1A:
                {
                    u32 Result = -(s32)DestData;
                    N = (Result < 0);
                    Z = (Result == 0);
                    V = 0;
                    C = (Result != 0);
                    *DestWrite = Result;
                } break;
            case 0x1B:
                {
                    // NOTE: No Operation
                } break;
            case 0x1C:
                {
                    u32 Result = ~DestData;
                    V = 0;
                    C = 0;
                    N = ((s32)Result < 0);
                    Z = (Result == 0);
                    *DestWrite = Result;
                } break;
            case 0x1D:
                {
                    u32 Result = DestData | SourceData;
                    C = 0;
                    V = 0;
                    Z = (Result == 0);
                    N = ((s32)Result < 0);
                    *DestWrite = Result;
                } break;
            case 0x1E:
                {
                    u32 Result = DestData - SourceData;
                    // TODO: Carry
                    C = 0;
                    N = ((s32)Result) < 0;
                    V = ((s32)DestData > 0 && (s32)SourceData > 0 && N) ||
                        ((s32)DestData < 0 && (s32)SourceData < 0 && !N);
                    Z = (Result == 0);
                    *DestWrite = Result;
                } break;
            case 0x1F:
                goto end_execution;
                break;
        }

        if (DestWrite - Memory == 0xbd) {
            printf("BallY = %d\n", *DestWrite);
            printf("VelocityY = %d\n", Memory[0xc0]);
        }
#if 0
        if (DestWrite - Memory == 0xbc) {
            printf("BallX = %d\n", *DestWrite);
            printf("VelocityX = %d\n", Memory[0xbf]);
        }
#endif

        Cycle++;
    }
end_execution:;

    exit(0);
    for (s32 MemoryIndex = 0; MemoryIndex <= MaxAddress; MemoryIndex++) {
        printf("@%03x\t%08x\n", MemoryIndex, Memory[MemoryIndex]);
    }
}

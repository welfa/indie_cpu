#!/bin/sh

gcc -lstdc++ -O0 -g -o assembler assembler.cpp
gcc -lstdc++ `sdl-config --cflags` `sdl-config --libs` -O0 -g -o simulator simulator.cpp
#gcc -lstdc++ `sdl2-config --cflags` `sdl2-config --libs` -O0 -g -o simulator_test simulator_test.cpp

 #include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#define internal static
#define global_variable static
#define local_persist static

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef s32 bool32;

typedef float real32;
typedef double real64;

struct token {
    s32 Start;
    s32 End;
};

struct parser {
    char *Contents;
    s32 Size;
    s32 LineNo;

    s32 CurrentOffset;
    u16 CurrentAddress;
};

struct string_table {
    u8 *Base;
    s32 SizeLeft;
};

internal string_table InitializeStringTable() {
    string_table Result;
    Result.Base = (u8 *)malloc(1 << 24);
    assert(Result.Base);
    Result.SizeLeft = 1 << 24;

    return Result;
}

internal char *PushStringTable(string_table *StringTable, s32 Size) {
    assert(StringTable->SizeLeft >= Size);
    char *Result = (char *)StringTable->Base;
    StringTable->Base += Size;
    StringTable->SizeLeft -= Size;

    return Result;
}

#define LABEL_LEN 50
#define MAX_LABELS 50
#define TokenLength(Token) (Token).End - (Token).Start

struct label {
    char Label[LABEL_LEN];
    u16 Address;
};

global_variable label Labels[MAX_LABELS];
global_variable s32 LabelCount = 0;

global_variable label UnresolvedLabels[MAX_LABELS];
global_variable s32 UnresolvedLabelCount = 0;

global_variable u32 Memory[4*1024];

internal void usage(char *ProgramName) {
    printf("Usage: %s filename.asm\n", ProgramName);
    exit(0);
}

inline bool32 IsDigit(char Character) {
    bool32 Result = ((Character >= '0') && (Character <= '9') ||
            Character == '-');

    return Result;
}

inline bool32 IsWhitespace(char Character) {
    bool32 Result = (Character == ' ') || (Character == '\t') ||
        (Character == '\r');

    return Result;
}

inline bool32 IsAlphabet(char Character) {
    bool32 Result = (Character >= 'A' && Character <= 'Z') ||
        (Character >= 'a' && Character <= 'z');

    return Result;
}

inline bool32 IsAlphaNumeric(char Character) {
    bool32 Result = IsAlphabet(Character) || IsDigit(Character) ||
        (Character == '_') || (Character == '.');

    return Result;
}

inline void ConvertToUpperCase(char *String) {
    while (*String) {
        if ((*String >= 'a') && (*String <= 'z')) {
            *String -= ('a' - 'A');
        }
        String++;
    }
}

internal token GetNextToken(parser *Parser) {
    char *CurrentChar = Parser->Contents + Parser->CurrentOffset;
    token Result;
    Result.Start = Parser->CurrentOffset;
    if (IsAlphaNumeric(*CurrentChar)) {
        while (!IsWhitespace(*CurrentChar)) {
            CurrentChar++;
            Parser->CurrentOffset++;
            if (Parser->CurrentOffset >= Parser->Size) {
                break;
            }
            if (!IsAlphaNumeric(*CurrentChar)) {
                break;
            }
        }
    } else {
        if (*CurrentChar == ';') {
            while (*CurrentChar != '\n') {
                Parser->CurrentOffset++;
                CurrentChar++;
            }
        } else {
            Parser->CurrentOffset++;
            CurrentChar++;
        }
    }
    Result.End = Parser->CurrentOffset;
    while (IsWhitespace(*CurrentChar)) {
        CurrentChar++;
        Parser->CurrentOffset++;
        if (Parser->CurrentOffset >= Parser->Size) {
            break;
        }
    }
    if ((Result.End - Result.Start == 1) &&
            Parser->Contents[Result.Start] == '\n') {
        Parser->LineNo++;
    }

    return Result;
}

internal void ParseLabel(parser *Parser, token Token) {
    label *Label = Labels + (LabelCount++);
    s32 LabelLength = Token.End - Token.Start;
    assert(LabelLength < ArrayCount(Label->Label));
    memcpy(Label->Label, Parser->Contents + Token.Start, LabelLength);
    Label->Address = Parser->CurrentAddress;
}

struct parse_digits_result {
    bool32 IsValid;
    u32 Digit;
};
internal parse_digits_result ParseDigits(parser *Parser, token Token) {
    parse_digits_result Result = {};
    Result.IsValid = true;
    if (Parser->Contents[Token.Start] == '0' &&
            Parser->Contents[Token.Start + 1] == 'x') {
        for (s32 CharIndex = 2; CharIndex < TokenLength(Token); CharIndex++) {
            Result.Digit *= 16;
            if (IsDigit(Parser->Contents[Token.Start + CharIndex])) {
                Result.Digit +=
                    (Parser->Contents[Token.Start + CharIndex] - '0');
            } else {
                char Digit = Parser->Contents[Token.Start + CharIndex];
                Digit = toupper(Digit);
                if (Digit >= 'A' && Digit <= 'F') {
                    Result.Digit += (Digit - 'A') + 10;
                } else {
                    Result.IsValid = false;
                    break;
                }
            }
        }
    } else {
        bool32 IsNegative = false;
        s32 Start = 0;
        if (Parser->Contents[Token.Start] == '-') {
            IsNegative = true;
            Start = 1;
        }
        for (s32 CharIndex = Start; CharIndex < TokenLength(Token); CharIndex++) {
            Result.Digit *= 10;
            if (IsDigit(Parser->Contents[Token.Start + CharIndex])) {
                Result.Digit +=
                    (Parser->Contents[Token.Start + CharIndex] - '0');
            } else {
                Result.IsValid = false;
                break;
            }
        }
        if (IsNegative) {
            Result.Digit = -Result.Digit;
        }
    }

    return Result;
}

struct parse_operand_result {
    u32 AddressType;
    u32 Register;
    u32 AddressOrImmediate;

    bool32 IsUnresolved;
    char Label[LABEL_LEN];
};
internal parse_operand_result ParseOperand(parser *Parser, token Token) {
    parse_operand_result Result = {};

    if (TokenLength(Token) == 1 && Parser->Contents[Token.Start] == '(') {
        Result.AddressType = 2;
        Token = GetNextToken(Parser);
    }

    if (TokenLength(Token) == 3 && Parser->Contents[Token.Start] == 'D' &&
            IsDigit(Parser->Contents[Token.Start + 1]) &&
            IsDigit(Parser->Contents[Token.Start + 2])) {
        Result.Register = (Parser->Contents[Token.Start + 1] - '0') * 10 +
            (Parser->Contents[Token.Start + 2] - '0');
    } else {
        parse_digits_result DigitResult = ParseDigits(Parser, Token);
        if (DigitResult.IsValid) {
            Result.AddressType = 3;
            Result.AddressOrImmediate = DigitResult.Digit;
        } else {
            Result.AddressType = 1;
            char Label[LABEL_LEN + 1] = {};
            memcpy(Label, Parser->Contents + Token.Start, TokenLength(Token));
            bool32 Found = false;
            for (s32 LabelIndex = 0; LabelIndex < LabelCount; LabelIndex++) {
                if (strcmp(Label, Labels[LabelIndex].Label) == 0) {
                    Found = true;
                    Result.AddressOrImmediate = Labels[LabelIndex].Address;
                }
            }
            Result.IsUnresolved = !Found;
            if (!Found) {
                strcpy(Result.Label, Label);
            }
        }
    }

    if (Result.AddressType == 2) {
        Token = GetNextToken(Parser);
        assert(TokenLength(Token) == 1 &&
                Parser->Contents[Token.Start] == ')');
    }

    return Result;
}

struct command_memory {
    bool32 Filled;
    u32 Operator;
    parse_operand_result SourceOperand;
    parse_operand_result DestOperand;
};

internal inline command_memory Parse2Operands(parser *Parser, token Token,
        token NextToken) {
    command_memory Result;
    parse_operand_result Dest = ParseOperand(Parser, NextToken);
    Token = GetNextToken(Parser);
    assert(TokenLength(Token) == 1 &&
            Parser->Contents[Token.Start] == ',');
    Token = GetNextToken(Parser);
    parse_operand_result Source = ParseOperand(Parser, Token);
    Result.SourceOperand = Source;
    Result.DestOperand = Dest;
    Result.Filled = true;

    return Result;
}

internal void ParseLine(parser *Parser) {
    token Token = GetNextToken(Parser);
    if (TokenLength(Token) == 1 && Parser->Contents[Token.Start] == '\n') {
    } else {
        token NextToken = GetNextToken(Parser);
        if (NextToken.End - NextToken.Start == 1) {
            if (Parser->Contents[NextToken.Start] == ',') {
                ParseLabel(Parser, Token);
                Token = GetNextToken(Parser);
                NextToken = GetNextToken(Parser);
                if (TokenLength(Token) == 1 && 
                        Parser->Contents[Token.Start] == '\n') {
                    Token = NextToken;
                    NextToken = GetNextToken(Parser);
                }
            }
        }

        u32 *CurrentMemory = Memory + Parser->CurrentAddress;
        command_memory CommandMemory = {};
        char Command[7] = {};
        s32 TokenLength = Token.End - Token.Start;
        if (TokenLength >= ArrayCount(Command) - 1) {
            char Buffer[50];
            assert(TokenLength < ArrayCount(Buffer) - 1);
            memcpy(Buffer, Parser->Contents + Token.Start, TokenLength);
            Buffer[TokenLength] = 0;
            fprintf(stderr, "Line: %d Command exceeded buffer size, read: \"%s\"\n", Parser->LineNo+1, Buffer);
            exit(1);
        }
        memcpy(Command, Parser->Contents + Token.Start, TokenLength);
        ConvertToUpperCase(Command);
        if (strcmp(Command, "ADD") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 0;
        } else if (strcmp(Command, ".DATA") == 0) {
            parse_digits_result DigitResult = ParseDigits(Parser, NextToken);
            assert(DigitResult.IsValid);
            *CurrentMemory = DigitResult.Digit;
        } else if (strcmp(Command, "AND") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 1;
        } else if (strcmp(Command, "ASL") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 2;
        } else if (strcmp(Command, "ASR") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 3;
        } else if (strcmp(Command, "BCC") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 4;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BCS") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 5;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BEQ") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 6;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BGE") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 7;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BGT") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 8;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BLE") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 9;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BLT") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 10;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BMI") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 11;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BNE") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 12;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BPL") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 13;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BVC") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 14;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BVS") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 15;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "BRA") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 16;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "CLR") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            CommandMemory.Operator = 17;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "CMP") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 18;
        } else if (strcmp(Command, "DIV") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 19;
        } else if (strcmp(Command, "XOR") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 20;
        } else if (strcmp(Command, "CALL") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            assert(Dest.AddressType == 1);
            CommandMemory.Operator = 22;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "RET") == 0) {
            CommandMemory.Operator = 23;
            CommandMemory.Filled = true;
            Parser->CurrentOffset = NextToken.Start;
        } else if (strcmp(Command, "MOV") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 24;
        } else if (strcmp(Command, "MUL") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 25;
        } else if (strcmp(Command, "NEG") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            CommandMemory.Operator = 26;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "NOP") == 0) {
            CommandMemory.Operator = 27;
            CommandMemory.Filled = true;
            Parser->CurrentOffset = NextToken.Start;
        } else if (strcmp(Command, "NOT") == 0) {
            parse_operand_result Dest = ParseOperand(Parser, NextToken);
            CommandMemory.Operator = 28;
            CommandMemory.DestOperand = Dest;
            CommandMemory.Filled = true;
        } else if (strcmp(Command, "OR") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 29;
        } else if (strcmp(Command, "SUB") == 0) {
            CommandMemory = Parse2Operands(Parser, Token, NextToken);
            CommandMemory.Operator = 30;
        } else if (strcmp(Command, "HLT") == 0) {
            CommandMemory.Operator = 31;
            CommandMemory.Filled = true;
            Parser->CurrentOffset = NextToken.Start;
        }

        if (CommandMemory.Filled) {
            if (CommandMemory.SourceOperand.IsUnresolved) {
                label *Unresolved = UnresolvedLabels + UnresolvedLabelCount++;
                strcpy(Unresolved->Label,
                        CommandMemory.SourceOperand.Label);
                Unresolved->Address = Parser->CurrentAddress;
            }
            if (CommandMemory.DestOperand.IsUnresolved) {
                label *Unresolved = UnresolvedLabels + UnresolvedLabelCount++;
                strcpy(Unresolved->Label,
                        CommandMemory.DestOperand.Label);
                Unresolved->Address = Parser->CurrentAddress;
            }
            *CurrentMemory = (CommandMemory.Operator & 0x1F) << 27 |
                (CommandMemory.SourceOperand.AddressType & 0x3) << 25 |
                (CommandMemory.SourceOperand.Register & 0x3F) << 20 |
                (CommandMemory.DestOperand.AddressType & 0x3) << 18 |
                (CommandMemory.DestOperand.Register & 0x3F) << 13 |
                ((CommandMemory.SourceOperand.AddressOrImmediate) |
                 (CommandMemory.DestOperand.AddressOrImmediate) & 0x1FFF);
        }

        Token = GetNextToken(Parser);
        if (TokenLength(Token) == 1 && 
                Parser->Contents[Token.Start] == '\n') {
        } else {
            char Buffer[1024];
            s32 TokenLength = Token.End - Token.Start;
            assert(TokenLength < ArrayCount(Buffer));
            memcpy(Buffer, Parser->Contents + Token.Start, TokenLength);
            Buffer[TokenLength] = 0;
            printf("Unexpected token: %s, expected newline.\n", Buffer);
            return;
        }

        Parser->CurrentAddress++;
    }
}

int main(int ArgCount, char *Arguments[]) {
    if (ArgCount == 1 || ArgCount > 2) {
        usage(Arguments[0]);
    }

    string_table StringTable = InitializeStringTable();

    char *Filename = Arguments[1];
    struct stat FileStat;

    int FileDescriptor = open(Filename, O_RDONLY);
    if (FileDescriptor < 0) {
        perror("Error opening file: ");
        exit(1);
    }
    if (fstat(FileDescriptor, &FileStat) < 0) {
        perror("Error getting stat of file: ");
        exit(1);
    }

    s32 FileSize = FileStat.st_size;
    char *Contents = PushStringTable(&StringTable, FileSize);
    if (read(FileDescriptor, Contents, FileSize) != FileSize) {
        perror("Error reading file: ");
        exit(1);
    }
    close(FileDescriptor);

    parser Parser;
    Parser.Contents = Contents;
    Parser.LineNo = 0;
    Parser.Size = FileSize;
    Parser.CurrentOffset = 0;
    Parser.CurrentAddress = 0;

    // Skip whitespaces
    char *CurrentChar = Parser.Contents;
    while (IsWhitespace(*CurrentChar)) {
        CurrentChar++;
        Parser.CurrentOffset++;
    }

    while (Parser.CurrentOffset < Parser.Size) {
        ParseLine(&Parser);
    }

    for (s32 UnresolvedIndex = 0; UnresolvedIndex < UnresolvedLabelCount;
            UnresolvedIndex++) {
        bool32 Found = false;
        for (s32 LabelIndex = 0; LabelIndex < LabelCount; LabelIndex++) {
            if (strcmp(Labels[LabelIndex].Label,
                        UnresolvedLabels[UnresolvedIndex].Label) == 0) {
                Found = true;
                Memory[UnresolvedLabels[UnresolvedIndex].Address] |=
                    Labels[LabelIndex].Address & 0xFFF;
                break;
            }
        }
        assert(Found);
    }

    for (s32 AddressIndex = 0; AddressIndex < Parser.CurrentAddress;
            AddressIndex++) {
        printf("@%03x\t%08x\n", AddressIndex, Memory[AddressIndex]);
    }
}

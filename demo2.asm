        clr D01
        clr D00
        clr D05
ClearScreenY,
        clr D03
ClearScreenX,
        mov D01, D03
        mov D04, D05
        mul D04, 320
        add D01, D04
        add D01, VRamStart
        mov (D01), 0xFF
        add D03, 1
        cmp D03, 320
        blt ClearScreenX
        add D05, 1
        cmp D05, 240
        blt ClearScreenY
MainLoop,
        clr D05
        add D05, BoxY
        sub D05, 5
        mov D07, D05
        add D07, 50
ClearOldY,
        clr D03
        add D03, BoxX
        sub D03, 5
        mov D08, D03
        add D08, 50
ClearOldX,
        mov D01, D03
        mov D04, D05
        mul D04, 320
        add D01, D04
        add D01, VRamStart
        mov (D01), 0xFF
        add D03, 1
        cmp D03, D08
        blt ClearOldX
        add D05, 1
        cmp D05, D07
        blt ClearOldY

        mov D10, VelocityX
        add BoxX, D10
        cmp BoxX, D00
        blt NegateX
        mov D10, MaxW
        cmp BoxX, D10
        blt CheckY
NegateX,
        neg VelocityX
CheckY,
        mov D10, VelocityY
        add BoxY, D10
        cmp BoxY, D00
        blt NegateY
        mov D10, MaxH
        cmp BoxY, D10
        blt DrawBoxY
NegateY,
        neg VelocityY

        clr D05
        add D05, BoxY
        mov D07, D05
        add D07, 50
DrawBoxY,
        clr D03
        add D03, BoxX
        mov D08, D03
        add D08, 50
DrawBoxX,
        mov D01, D03
        mov D04, D05
        mul D04, 320
        add D01, D04
        add D01, VRamStart
        mov (D01), 0xE0
        add D03, 1
        cmp D03, D08
        blt DrawBoxX
        add D05, 1
        cmp D05, D07
        blt DrawBoxY
        mov D10, NopLoopIndex
NopLoop,
        nop
        sub D10, 1
        cmp D10, D00
        bge NopLoop

        bra MainLoop

VRamStart, .DATA 0x1000
BoxX, .DATA 50
BoxY, .DATA 50
MaxW, .DATA 270
MaxH, .DATA 190
VelocityX, .DATA 1
VelocityY, .DATA 1
NopLoopIndex, .DATA 100

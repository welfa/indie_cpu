//http://tinyvga.com/vga-timing/640x480@60Hz
//
//Row,Colは定数だけど・・・
module VGA #(parameter Row = 480,Col = 640) (
        input                   clk25, reset,
        output  reg             hsync,   vsync,
        output VideoOn,
        output  reg             reading,
        output  reg     [9:0]  hcounter,
        output  reg     [9:0]  vcounter);
    localparam
        hva     = Col,
        hfp     = 16,
        hsp     = 96,
        hbp     = 48,
        hwhole  = hva + hfp + hsp + hbp,
        vva     = Row,
        vfp     = 10,
        vsp     = 2,
        vbp     = 33,
        vwhole  = vva + vfp + vsp + vbp;
    assign
        VideoOn = (hcounter < hva && vcounter < vva) ? 1'b1 : 0;

    always @(posedge clk25) begin
        if (reset) begin
            hcounter <= 0;
            vcounter <= 0;
        end else begin
            hcounter <= (hcounter < hwhole-16'b1) ? hcounter+16'b1 : 16'b0;
            vcounter <=
                (hcounter < hwhole-16'b1) ? vcounter :
                (vcounter < vwhole-16'b1) ? vcounter+16'b1 : 16'b0;
            hsync <= (hva+hfp<=hcounter && hcounter<hwhole-hbp) ? 16'b0 : 16'b1;
            vsync <= (vva+vfp<=vcounter && vcounter<vwhole-vbp) ? 16'b0 : 16'b1;
            reading <= (16'b0<=vcounter && vcounter<vva) ? 16'b1 : 16'b0;
        end
    end
endmodule

        clr D00
        clr D01
        mov D01, VideoIndex
        add D01, VRamStart
        mov D31, 10
        neg D31
LoopTmp,
        clr D05
ClearScreenY,
        clr D03
ClearScreenX,
        mov D01, D03
        mov D04, D05
        mul D04, 320
        add D01, D04
        add D01, VRamStart
        mov (D01), 0x92
        add D03, 1
        cmp D03, 320
        blt ClearScreenX
        add D05, 1
        cmp D05, 240
        blt ClearScreenY
        cmp ClearedBuffer, D00
        bne LoopVRam
        mov D20, 1
        mov ClearedBuffer, D20
        ret
        bra LoopTmp

LoopVRam,
        mov D05, OldBallX
        add D05, 90
        mov D06, OldBallY
        add D06, 90
        cmp D06, 240
        ble B1
        mov D06, 240
B1,
        cmp D05, 320
        ble B2
        mov D05, 320
B2,
        mov D04, OldBallY
        sub D04, 90
ClearOldBallY,
        mov D03, OldBallX
        sub D03, 90
ClearOldBallX,
        mov D01, D03
        mov D07, D04
        mul D07, 320
        add D01, D07
        mov (D01), 0x92
        add D03, 1
        cmp D03, D05
        blt ClearOldBallX
        add D04, 1
        cmp D04, D06
        blt ClearOldBallY

DrawNewBall,
        mov D02, BallX
        add D02, 80
        mov D08, BallY
        add D08, 80
        mov D06, BallY
        sub D06, 80
        cmp D08, 240
        ble B3
        mov D08, 240
B3,
        cmp D02, 320
        ble DrawNewBallY
        mov D02, 320
DrawNewBallY,
        mov D04, BallX
        sub D04, 80
DrawNewBallX,
        mov D05, D04
        mov D03, D06
        mov D01, D04
        mov D07, D06
        mul D07, 320
        add D01, D07

        sub D03, BallY
        mov D11, D03
        mov D28, D03
        mul D03, D03
        mov D10, D05
        sub D05, BallX
        mov D27, D05
        mul D05, D05
        mov D07, D03
        add D07, D05

        cmp D07, BallR2
        bgt Background

        mov D12, D10
        asr D12, 2
        neg D12
        mov D13, D11
        asr D13, 2
        mov D15, D10
        mov D16, D11
        add D15, D13
        add D16, D12

        mov D20, BallR2
        sub D20, D03
        sub D20, D05

        asr D20, 5

        mov D22, 1
        mov D23, 1
        add D23, D20
        asr D23, 1
        mov D21, D23
        sub D23, D22
        mul D23, D23
        cmp D23, 1
        ble Loop2
Loop1,
        mov D22, D21
        mov D23, D20
        div D23, D22
        add D23, D22
        asr D23, 1
        mov D21, D23
        sub D23, D22
        mul D23, D23
        cmp D23, 1
        bgt Loop1

Loop2,
        mov D23, D21
        mul D23, D23
        cmp D23, D20
        ble ExitSqrtLoop
        sub D21, 1
        bra Loop2

ExitSqrtLoop,
        cmp D21, 0
        beq Sample
        cmp D27, 0
        bge PosD27
        neg D27
        div D27, D21
        neg D27
        bra CheckD28
PosD27,
        div D27, D21
CheckD28,
        cmp D28, 0
        bge PosD28
        neg D28
        div D28, D21
        neg D28
        bra AddNormal
PosD28,
        div D28, D21
AddNormal,
        add D15, D27
        add D16, D28

Sample,
        asr D15, 5
        and D15, 1
        asr D16, 5
        and D16, 1
        beq EvenRow
OddRow,
        cmp D15, 0
        beq Red
        bra White
EvenRow,
        cmp D15, 0
        beq White
        bra Red
Red,
        mov (D01), 0xE0
        bra Render
White,
        mov (D01), 0xFF
        bra Render
Background,
        mov (D01), 0x92
Render,
        add D04, 1
        cmp D04, D02
        blt DrawNewBallX
        add D06, 1
        cmp D06, D08
        blt DrawNewBallY

Loop,
        mov D03, BallY
        mov OldBallY, D03
        add D03, VelocityY
        add D03, 1
        mov BallY, D03
        mov D03, VelocityY
        add D03, 1
        mov VelocityY, D03
        mov D03, BallY
        cmp D03, 140
        blt ChangeBallX
        mov VelocityY, D31

ChangeBallX,
        mov D09, BallX
        mov OldBallX, D09
        add D09, VelocityX
        cmp D09, 160
        bgt NegateVelocity
        cmp D09, 40
        blt NegateVelocity
        bra MoveBall
NegateVelocity,
        neg VelocityX
MoveBall,
        mov BallX, D09
        ret
        bra LoopVRam
        hlt
VRamStart, .DATA 0x00001000
VideoIndex, .DATA 76799
OldBallX, .DATA 100
OldBallY, .DATA 120
BallX, .DATA 100
BallY, .DATA 120
BallR2, .DATA 6400
VelocityX, .DATA 3
VelocityY, .Data 0
ClearedBuffer, .DATA 0

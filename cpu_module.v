`include "def_indie.v"

//synchronous RAM model
/// addr_width = 12;
/// data_width = 32;
module DPRAM_SYNC #(parameter W=32, SIZE=1024*4) (
    input       [W-1:0]  din_dst,
    input       [11:0]  addr_src,   addr_dst,
    input               we_dst,     clk,
    output reg  [W-1:0]  dout_src,   dout_dst);
    //localparam mem_size = 1024*4;
    reg     [W-1:0]  ram[0:SIZE-1];

    initial $readmemh(`MEM_INIT_FILE, ram);

    //Port src
    always @ (posedge clk) begin
        dout_src <= ram[addr_src];
    end

    //Port dst
    always @ (posedge clk) begin
        if(we_dst) begin
            ram[addr_dst] <= din_dst;
            dout_dst <= din_dst;
        end else begin
            dout_dst <= ram[addr_dst];
        end
    end
endmodule

//for vram
module RAM_SYNC #(parameter SIZE=320*240) (
    input               clk,    /// clock
    input               we,     /// write enable
    input       [31:0]  addr,   /// address input
    input       [7:0]   din,    /// data input
    output  reg [7:0]   dout);  /// data output

    reg     mem[0:SIZE-1];

    always @ (posedge clk) begin
        if(we) mem[addr] <= din;
        dout <= mem[addr];
    end
endmodule

module INST_PARSER (
    input   [31:0]  instraction,
    output  [4:0]   inst,
    output  [1:0]   ref_src,        ref_dst,
    output  [4:0]   reg_addr_src,   reg_addr_dst,
    output  [11:0]  addrimm);
    assign  inst        = instraction[31:27],
            ref_src     = instraction[26:25],
            reg_addr_src= instraction[24:20],
            ref_dst     = instraction[19:18],
            reg_addr_dst= instraction[17:13],
            //12
            addrimm     = instraction[11: 0];
endmodule

module CLK_MANAGER #(parameter DIV_RATE=3)(
    input               clk_orig,
    output              clk_mult,
    output  reg         clk_main,
    output  reg [7:0]   clk_onehot,
                        clk_onehot_rd, //ready
    input               com_rst);
    initial clk_onehot      = 8'b1;
    initial clk_onehot_rd   = 8'b1;

    reg [3:0] counter = 4'b0;
    assign clk_mult = clk_orig;

    always @(posedge clk_orig) begin
        if(~com_rst) begin
            clk_onehot <= clk_onehot_rd;
            clk_main <= counter==4'b0 ? 1'b1 : 1'b0;
            counter <= counter<(DIV_RATE-1) ? counter+4'b1 : 4'b0;
        end else begin
            clk_onehot <= 8'b1;
            counter <= 4'b0;
        end
    end
    always @(negedge clk_orig) begin
        clk_onehot_rd <= 8'b1 << counter;
    end
endmodule

module  PC_CONTROLLER (
    input   [4:0]   inst,
    //Neg Zero oVerflow Clear
    input   [3:0]   alu_flag,
    //0 for PC<=PC+4,   1 for imm
    output          pc_sel);

    wire
        _n = alu_flag[3],
        _z = alu_flag[1],
        _v = alu_flag[0],
        _c = alu_flag[2];

    assign pc_sel = pcselmap(inst, alu_flag, _n, _z, _v, _c);

    //inst読んでフラグチェックしてselを駆動
    function [0:0] pcselmap(
        input [4:0] i,
        input [3:0] f,
        input n, z, v, c);
    case(i)
        `IBCC:      pcselmap = ~c;
        `IBCS:      pcselmap = c;
        `IBEQ:      pcselmap = z;
        `IBGE:      pcselmap = ~^{n,v};
        `IBGT:      pcselmap = ~z & ~^{n,v};
        `IBLE:      pcselmap = z | ^{n,v};
        `IBLT:      pcselmap = ^{n,v};
        `IBMI:      pcselmap = n;
        `IBNE:      pcselmap = ~z;
        `IBPL:      pcselmap = ~n;
        `IBUC:      pcselmap = ~v;
        `IBUS:      pcselmap = v;
        `IBRA:      pcselmap = 1'b1;
        default:    pcselmap = 1'b0;
    endcase
    endfunction
endmodule

module ALU_WRAPPER (
    input       [4:0]   alu_ctrl,
    input       [31:0]  in1, in2,
    input           clk,
    input   [7:0]   clk_onehot,
    output  [31:0]  out,
    output  [3:0]   flags,
    output  [3:0]   flags_nxt,
    input           com_rst);

    wire isnop = alu_ctrl==`INOP;
    wire ismov = alu_ctrl==`IMOV;
    //NOP: result = 0+0, MOV: result = src+0
    wire [4:0]  ialu_ctrl = (isnop | ismov) ? `IADD : alu_ctrl;
    wire [3:0]  iflags;
    wire [31:0] _in1 = ismov ? 32'b0 : in1;
    alu inneralu (_in1, in2, ialu_ctrl, out, iflags);

    wire [3:0] flags_keepdrive =
        (isnop) ?
            flags : //keep
            (ismov) ? {flags[3], 1'b0, flags[1], 1'b0} :
            iflags; //drive
    REG_LCI_NXT #4 reg_flags (
        clk, `c1,
        flags_keepdrive,
        flags, flags_nxt,
        clk_onehot[1], com_rst, `c0);  //posedge onehot[2]で書かれる
endmodule


module MAIN_CONTROLLER (
    input   [4:0]   inst,
    input   [7:0]   clk_onehot,
    input   [1:0]   ref_src, ref_dst,
    //0 for add/imm,    1 for reg[ad],      2 for 0
    output  [1:0]   ram_sel_src, ram_sel_dst,
    //0 for M[reg[ad]], 1 for add/imm,      2 for reg[ad],      3 for 0
    output  [1:0]   alu_sel_src, alu_sel_dst,
    //ほぼinst
    input   [31:0]  reg_out_dst,
    output reg      vramaccess,
    output  [4:0]   alu_ctrl,
    output          ram_we, reg_we,
    output reg      vga_swp);

    //instによってはALUを触りたくないのがあるので
    //(branch zero flag setとか)適当にNOP
    // -> aluは自体はレジスタを持たないようにした．
    //    それに伴ってINOPは反応しないのでIADD&ダミー入力入れる
    //    この処理はaluwrapperがやる．
    wire alunop =
        (`IBCC<=inst&&inst<=`IBRA) ||   //branch系
        inst == `ILEA ||
        inst == `INOP ||
        inst == `ISWP ||
        inst == `IHLT;
    assign alu_ctrl = alunop ? `INOP : inst;
    always @(posedge clk_onehot[1]) begin
        //ISWPは2回立て続けは許可しない
        vga_swp <= inst == `ISWP;
        vramaccess <=
            (inst==`IMOV && ref_dst==2'b10 && (|reg_out_dst[31:12]));
    end

    assign
        ram_sel_src = ramselmap(ref_src),
        ram_sel_dst = ramselmap(ref_dst),
        //alunopで入力をダミー(0)にする
        //本来ALUにやらせる仕事だけど不定値になってほしくないため
        alu_sel_src = alunop ? 2'b11 : aluselmap(ref_src),
        alu_sel_dst = alunop ? 2'b11 : aluselmap(ref_dst);

    //書き戻しが必要ならH
    wire wb = ~(
        (`IBCC<=inst&&inst<=`IBRA) ||   //branch系
        inst == `ICMP ||
        inst == `ILEA ||
        inst == `INOP ||
        inst == `ISWP ||
        inst == `IHLT   );
    //instによっては書き戻しが必要ないので(branchとか)適当に無効化
    //posedge clk_onehot[2]で書かれる
    assign
        ram_we = (wb &&  ^ref_dst && clk_onehot[1]) ? 1'b1 : 1'b0,
        reg_we = (wb && ~|ref_dst && clk_onehot[1]) ? 1'b1 : 1'b0;

    function [1:0] ramselmap (input [1:0] ref1);
        case(ref1)
            2'b00:  ramselmap = 2'b10;
            2'b01:  ramselmap = 2'b00;
            2'b10:  ramselmap = 2'b01;
            2'b11:  ramselmap = 2'b00;
        endcase
    endfunction
    function [1:0] aluselmap (input [1:0] ref2);
        case(ref2)
            2'b00:  aluselmap = 2'b10;
            2'b01:  aluselmap = 2'b00;
            2'b10:  aluselmap = 2'b00;
            2'b11:  aluselmap = 2'b01;
        endcase
    endfunction

endmodule

module GEN_REGS #(parameter REGNUM=32) (
    input   [4:0]   addr_src, addr_dst,
    input   [31:0]  din_dst,
    input           we_dst,
    input           clk,
    output  [31:0]  dout_src, dout_dst,
    input           com_rst);
    //クロック非同期(yaba)
    //posedge clk_onehot[2]と同期してwe_dstがHなら書き込み
    //

    reg     [31:0]  regs[0:REGNUM-1];
    assign  dout_src = com_rst ? 32'b0 : regs[addr_src],
            dout_dst = com_rst ? 32'b0 : regs[addr_dst];

    always @(posedge clk) begin
        if(we_dst)
            regs[addr_dst] <= din_dst;
    end
    endmodule

module REG_LCI_NXT #(parameter DATA_WIDTH=32) (
    input                       clk, en,
    input   [DATA_WIDTH-1:0]    din,
    output  [DATA_WIDTH-1:0]    dout, dout_nxt,
    input                       ld, clr, inr);
    assign dout_nxt = (clr) ? 0 : (ld) ? din : (inr) ? dout + 1 : dout;
    REG_DFF #(DATA_WIDTH) R0 (clk, en, dout_nxt, dout);
endmodule

//clock-enable register model
module REG_DFF #(parameter DATA_WIDTH=32) (
    input                           clk, en,
    input       [DATA_WIDTH-1:0]    din,
    output reg  [DATA_WIDTH-1:0]    dout);
    always @ (posedge clk)
        if(en) dout <= din;
endmodule

//---------------------------
module SEG7_ENC (data, enc_out);
    input  [3:0] data;
    output [6:0] enc_out;
    reg    [6:0] enc_out_n;

    assign enc_out = ~enc_out_n;

    always @(data)
    case(data)
        4'h0:    enc_out_n = `SEG_7_0;
        4'h1:    enc_out_n = `SEG_7_1;
        4'h2:    enc_out_n = `SEG_7_2;
        4'h3:    enc_out_n = `SEG_7_3;
        4'h4:    enc_out_n = `SEG_7_4;
        4'h5:    enc_out_n = `SEG_7_5;
        4'h6:    enc_out_n = `SEG_7_6;
        4'h7:    enc_out_n = `SEG_7_7;
        4'h8:    enc_out_n = `SEG_7_8;
        4'h9:    enc_out_n = `SEG_7_9;
        4'ha:    enc_out_n = `SEG_7_A;
        4'hb:    enc_out_n = `SEG_7_B;
        4'hc:    enc_out_n = `SEG_7_C;
        4'hd:    enc_out_n = `SEG_7_D;
        4'he:    enc_out_n = `SEG_7_E;
        4'hf:    enc_out_n = `SEG_7_F;
        default: enc_out_n = `SEG_7_UNDEFINED;
    endcase
endmodule

module SEG7_DEC (hex, dec_out);
    input  [6:0] hex;
    output [4:0] dec_out;
    reg    [4:0] dec_out;

    always @(hex)
    case(~hex)
        `SEG_7_0 : dec_out = 5'h00;
        `SEG_7_1 : dec_out = 5'h01;
        `SEG_7_2 : dec_out = 5'h02;
        `SEG_7_3 : dec_out = 5'h03;
        `SEG_7_4 : dec_out = 5'h04;
        `SEG_7_5 : dec_out = 5'h05;
        `SEG_7_6 : dec_out = 5'h06;
        `SEG_7_7 : dec_out = 5'h07;
        `SEG_7_8 : dec_out = 5'h08;
        `SEG_7_9 : dec_out = 5'h09;
        `SEG_7_A : dec_out = 5'h0a;
        `SEG_7_B : dec_out = 5'h0b;
        `SEG_7_C : dec_out = 5'h0c;
        `SEG_7_D : dec_out = 5'h0d;
        `SEG_7_E : dec_out = 5'h0e;
        `SEG_7_F : dec_out = 5'h0f;
        default  : dec_out = 5'h1f; /// error
    endcase
endmodule

//
///*********************** 2to4 decoder model *************************/
//
module dec_2to4 (din, dout, en);

    input  [1:0] din;
    input        en;
    output [3:0] dout;

    assign dout[0]  = en & (din == 2'b00);
    assign dout[1]  = en & (din == 2'b01);
    assign dout[2]  = en & (din == 2'b10);
    assign dout[3]  = en & (din == 2'b11);
endmodule
//
///*********************** 3to8 decoder model *************************/
//
module dec_3to8 (din, dout, en);

    input  [2:0] din;
    input        en;
    output [7:0] dout;

    dec_2to4 D0 (din[1:0], dout[3:0], en & ~din[2]);
    dec_2to4 D1 (din[1:0], dout[7:4], en & din[2]);
endmodule
//
///*********************** 4to16 decoder model *************************/
//
module dec_4to16 (din, dout, en);

    input  [3:0] din;
    input        en;
    output [15:0] dout;

    dec_3to8 D0 (din[2:0], dout[7:0], en & ~din[3]);
    dec_3to8 D1 (din[2:0], dout[15:8], en & din[3]);
endmodule

//
`default_nettype none
//`define PROGRAM_ENTRY_POINT	32'h0010

///*********************** Input files *************************/

`define PROGRAM_NAME        "demo2"
`define MEM_INIT_FILE       {`PROGRAM_NAME, ".mem"}

/*************************************************************/

`define c0      1'b0
`define c1      1'b1

/********************* CPU instraction ***********************/

//o:set/clear, s:always set, z:always clear, x:keep
//                        nzvc  no writeback
`define IADD    5'h00   //oooo  
`define IAND    5'h01   //oozz  
`define IASL    5'h02   //oooo  
`define IASR    5'h03   //oooo  
`define IBCC    5'h04   //      x
`define IBCS    5'h05   //      x
`define IBEQ    5'h06   //      x
`define IBGE    5'h07   //      x
`define IBGT    5'h08   //      x
`define IBLE    5'h09   //      x
`define IBLT    5'h0a   //      x
`define IBMI    5'h0b   //      x
`define IBNE    5'h0c   //      x
`define IBPL    5'h0d   //      x
`define IBUC    5'h0e   //      x
`define IBUS    5'h0f   //      x
`define IBRA    5'h10   //      x
`define ICLR    5'h11   //zszz  
`define ICMP    5'h12   //oooo  x
`define IDIV    5'h13   //oooz  
`define IXOR    5'h14   //oozz  
`define ILEA    5'h15   //xxxx  x


`define ISWP    5'h17   //xxxx  
`define IMOV    5'h18   //oozz  
`define IMUL    5'h19   //oooz  
`define INEG    5'h1a   //oooo  
`define INOP    5'h1b   //xxxx  x
`define INOT    5'h1c   //oozz  
`define IOR     5'h1d   //oozz  
`define ISUB    5'h1e   //oooo  
`define IHLT    5'h1f   //xxxx  x

/*********************** CPU control *************************/
`define COM_RST 2'b00
`define COM_RUN 2'b01

/*********************** memory probe *************************/
`define MEM_DATA  4'h0
`define MEM_END   4'hf

/*********************** 7-segment LED ************************
7-segment LED bit assignment :
      00000
     5     1
     5     1
     5     1
      66666
     4     2
     4     2
     4     2
      33333
***************************************************************/
`define SEG_7_0	7'h3f			///	 543210 : 0
`define SEG_7_1 7'h06			///	    21  : 1
`define SEG_7_2 7'h5b			///	6 43 10 : 2
`define SEG_7_3 7'h4f			///	6  3210 : 3
`define SEG_7_4 7'h66			///	65  21  : 4
`define SEG_7_5 7'h6d			///	65 32 0 : 5
`define SEG_7_6 7'h7d			///	65432 0 : 6
`define SEG_7_7 7'h07			///	    210 : 7
`define SEG_7_8 7'h7f			///	6543210 : 8
`define SEG_7_9 7'h6f			///	65 3210 : 9
`define SEG_7_A	7'h77			///	654 210 : A
`define SEG_7_B 7'h7c			///	65432   : b
`define SEG_7_C 7'h39			///	 543  0 : C
`define SEG_7_D 7'h5e			///	6 4321  : d
`define SEG_7_E 7'h79			///	6543  0 : E
`define SEG_7_F 7'h71			///	654   0 : F
`define SEG_7_UNDEFINED	7'h40	///	6       : -

